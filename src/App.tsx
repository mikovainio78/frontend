import React from 'react'
import Routes from './routes'
import GlobalStyles from './styles/globalStyles'

function App() {
  return (
    <div className="App">
      <GlobalStyles />
      <header test-id="LoginComponent" className="App-header">
        <Routes />
      </header>
    </div>
  )
}

export default App
