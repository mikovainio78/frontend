import React from 'react'
import { useHistory } from 'react-router-dom'
import logo from '../../assets/jackpot.svg'
import './styles.ts'
import 'react-toastify/dist/ReactToastify.css'
import { DivButton, Header, Image, Wrapper } from './styles'

const Login = () => {
  const history = useHistory()

  const handleLogin = () => {
    history.push('/dashboard')
  }

  return (
    <Wrapper>
      <Header>
        <h1>Cryptostars</h1>
        <Image src={logo} alt="logo" />
        <p>You can have your own star</p>
        <DivButton className="Login-go-btn" onClick={handleLogin}>
          <p>Enter this universe</p>
        </DivButton>
      </Header>
    </Wrapper>
  )
}

export default Login
